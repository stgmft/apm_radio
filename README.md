<h1 align="center">📻 Online radio site</h1>  
<h2><a href="https://apm-radio.herokuapp.com/">apm-radio.herokuapp.com</a></h2>

<h2>Overview</h2>
<p><strong>This is application for personal online radio page with live chat.</strong><br><br> Written in Ruby 3.0.0 (Rails 6). It has 
audio widget that translates radio stream or podcast. I used <a href="https://zeno.fm/">Zeno fm </a> to host test audio file.
The project has authentication system that allows to unblock live chat where users can discuss current stream.<br> Site also has news section so you can add articles there.
Made with responsive design. <br>Inspired by the Michael Hartl's <a href="https://www.railstutorial.org/">Ruby on Rails Tutorial</a></p>

<h2>Example usage</h2>
On the main page visitor see the audio player, he can listen to the podcast or live stream. In the header of the site visitor is 
invited to connect to the site to get access to the live chat. Lower locates the news section. 
So if user wants to participate in discussion he either signs in or creates new account. After that he is redirected to the home page
and here live chat appears. In the left tab there is the list of users online, in the right tab the messages. As usual user can type 
his message and send it. He will see that his message has appeared in the right tab.

<h2>Built with</h2>
<ul>
<li><a href="https://guides.rubyonrails.org/">Ruby on Rails 6</a></li>
<li><a href="https://www.postgresql.org/">PostgreSQL</a></li>
<li><a href="https://redis.io/">Redis</a></li>
<li><a href="https://sidekiq.org/">Sidekiq</a></li>
<li><a href="https://github.com/rails/webpacker">Webpacker</a></li>
<li><a href="https://getbootstrap.com/">Bootstrap</a></li>
<li><a href="https://github.com/inuyaksa/jquery.nicescroll">JQuery nicescroll</a></li>
<li><a href="https://getbootstrap.com/">Bootstrap</a></li>
</ul>

<h2>Developer Info</h2>
The site is made with RoR 6 and Vanilla JS. All styles are processed with webpacker and locate in app/javascript/stylesheets.
For live chat application uses Rails ActionCable and redis. For sending messages application uses ActiveJob and Sidekiq as adapter. 
Redis configuration is different for development and production stages. Uncomment lines 6,7 in config/initializers/redis.rb for development. 

<h2>Getting started</h2>
<h3>Prerequisites</h3>
<ul>
<li> Heroku user account: <a href="https://signup.heroku.com/devcenter">Sign up for free</a></li>
<li> Installed <a href="https://git-scm.com/downloads">Git client</a> and Heroku <a href="https://devcenter.heroku.com/articles/heroku-cli#install-the-heroku-cli">CLI</a>
</ul>

<h3>Deployment</h3>
<p>1. Clone repository on your local machine and change directory to cloned repo</p> 

$<strong> git clone git@bitbucket.org:stgmft/apm_radio.git</strong>
<p>2. Create Heroku app</p>

$<strong> heroku create -a example-app</strong>

<p>3. Push code on heroku </p>

$<strong> git push heroku</strong>

<p>4. Add Redis ToGo add-on <a href="https://devcenter.heroku.com/articles/managing-add-ons">(Managing Add-ons on Heroku)</a>

<p>5. In the dashboard on heroku go in the Resources section and activate worker (Sidekiq) with toggle</p>

<p>6. Run db migrations</p>

$<strong> heroku run rake db:migrate</strong>

<p>7. Create default chatroom</p>

$<strong> heroku run rails console</strong><br>

irb(main)<strong> Chatroom.create(title: "Default")</strong>

<h3>Webpack public packs clean up </h3>
<p>Each time you start rails application webpack compiles javascript files (webpack asset) in the <strong>public/packs</strong> folder</p>
<p>You could run <strong>$ bundle exec rails webpacker:clobber</p>

<p>Install <strong>clean-webpack-plugin</strong></p>

<em>$ yarn add -D clean-webpack-plugin</em>

<p>Add plugin to development.js</p>
<p># webpack/development.js<br>
const CleanWebpackPlugin = require("clean-webpack-plugin")<br>
const path = require("path")<br>
const environment = require("./environment")<br>

environment.plugins.append(<br>
  "CleanWebpackPlugin",<br>
  new CleanWebpackPlugin(["packs"], {<br>
    root: path.resolve(__dirname, "../../public"),<br>
    verbose: true<br>
  })<br>
)<br>
module.exports = environment.toWebpackConfig()</p>


<h2> Contact me</h2>
<a href="https://linkedin.com/in/andrei-zheksim/"><strong>LinkedIn</strong></a>
<p>Email: andrewsanser@gmail.com<p>
 
 <h2>License</h2>
 
 MIT License
