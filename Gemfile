source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.0'
gem 'rails', '~> 6.0.3', '>= 6.0.3.5'
gem 'bcrypt', '3.1.13'
gem 'pg', '>= 0.18', '< 2.0'
gem 'faker', '2.11.0'
gem 'puma', '~> 5'
gem 'sass-rails', '>= 6'
gem 'rexml'
gem 'webpacker', '3.1.1'
gem 'redis'
gem 'redis-namespace'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.7'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'sidekiq', '~> 5.2.8'
gem 'sinatra'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 5.0.0'
  gem 'factory_bot_rails'
end

group :development do
  gem 'web-console', '~> 4.1.0'
  gem 'listen', '~> 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '~> 3.34'
  gem 'capybara-screenshot'
  gem 'selenium-webdriver'
  gem "show_me_the_cookies"
  gem 'webdrivers'
  gem 'rails-controller-testing', '~> 1.0.4'
  gem 'minitest'
  gem 'minitest-reporters'
  gem 'mini_backtrace'
  gem 'shoulda-matchers', '~> 4.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
