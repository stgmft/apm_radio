require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Главная | APM radio"
  end

  test "should get archive" do
    get archive_path
    assert_response :success
    assert_select "title", "Архив | APM radio"
  end

end
