require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "layouts links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", archive_path, count: 2
  end
end
