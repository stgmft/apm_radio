import consumer from "./consumer"

document.addEventListener("turbolinks:load", () => {
  console.log('turbolinks load')
  consumer.subscriptions.create("AppearanceChannel", {
    connected() {
      // Called when the subscription is ready for use on the server
      console.log('connected to appearance_channel')
      // subscribes only on the index page with radiochat
      this.chatIsPresent ? this.appear() : console.log('away')//this.away()
    },

    disconnected() {
      // Called when the subscription has been terminated by the servers
    },

    received(data) {
      // let usersUl = document.querySelector('.users-online')
      if (data['action'] == "appear") {
        console.log("appeared user: " + data['appeared_user'])
        // prevent from re-adding user
        this.addUser(data)
      }
      if (data['action'] == "disappear") {
        console.log("disappeared user: " + data['disappeared_user'])
        this.removeUser(data)
      }
    },

    chatIsPresent() {
      const radioChat = document.querySelector('.radiochat')
      console.log('chat is present: ' + !!radioChat)
      return !!radioChat
    },

    appear() {
      this.perform("appear")
      console.log('appear() is called')
    },

    away() {
      this.perform("away")
      console.log('away() is called')
    },

    template(data) {
      return `<li class="list-group-item">
      <a href="#">
      <div class="d-flex flex-row">
      <div class="avatar mr-2">MH</div><span class="my-auto">${data}</span>
      </div>
      </a>
      </li>`
    },

    addUser(data) {
      const usersUl = document.querySelector('.users-online')
      const usersCollection = usersUl.getElementsByTagName('span')
      const usersList = Array.from(usersCollection, item => item.innerText)
      const userInList = usersList.find(u => u == data['appeared_user'])
      if (userInList==undefined) {
        usersUl.insertAdjacentHTML('beforeend', this.template(data['appeared_user']))
      }
    },

    removeUser(data) {
      const usersUl = document.querySelector('.users-online')
      const usersCollection = usersUl.getElementsByTagName('span')
      const usersList = Array.from(usersCollection, item => item.innerText)
      console.log(usersList)
      const usersIndex = usersList.findIndex(u => u == data['disappeared_user'])
      if (usersIndex >= 0) {
        usersUl.getElementsByTagName('li')[usersIndex].remove()
      }
    }
  });
})
