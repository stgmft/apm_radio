import consumer from "./consumer"

document.addEventListener('turbolinks:load', () => {
  const radiochat = document.querySelector('.radiochat')
  if (radiochat) {
    const chatChannel = consumer.subscriptions.create("ChatChannel", {
      connected() {
        // Called when the subscription is ready for use on the server
        console.log('connected to chatChannel');
      },

      disconnected() {
        // Called when the subscription has been terminated by the server
      },

      received(data) {
        // Called when there's incoming data on the websocket for this channel
        let chat = document.querySelectorAll('.radiochat')[1];
        let strip = chat.querySelector('.strip');
        strip.insertAdjacentHTML('beforebegin', this.template(data));
        console.log('receive(dat)')
      },

      template(data) {
        let current_user = document.querySelector('.radiochat').dataset.currentUser
        if (data.sent_by == current_user) {
          return `<div class="row right d-flex justify-content-end">
            <div class="text w-75 text-right">
              <h4>${data.sent_by}</h4>
              <p class="msg-text">
                ${data.body}
              </p>
            </div>
            <div class="wrapper d-flex mx-2">
              <div class="avatar align-self-end">ZW</div>
            </div>
            <div class="col-2 col-sm-4"></div>
          </div>`
        } else {
          return `<div class="row left d-flex flex-row">
            <div class="wrapper d-flex mx-2">
              <div class="avatar align-self-end" style="border-color: yellow;">ZW</div>
            </div>
            <div class="text w-75">
              <h4>${data.sent_by}</h4>
              <div class="status offline"></div>
              <p class="msg-text">
                ${data.body}
              </p>
            </div>
            `
        }
      },

    })
    let form = document.querySelector('form.chat-msg')
    if (form) {
      // send by mouse click
      let btn = $('.send-button');
      let message = form.querySelector('.message-input');
      console.log(btn);
      btn.click(()=> chatChannel.send({body: message.value}));
      message.value = "";
      // send by Enter
      form.addEventListener('submit', (e) => {
        e.preventDefault();
        let message = form.querySelector('.message-input')
        if (message.value == "") return;
        chatChannel.send({body: message.value})
        message.value = ""
      })
    }
  }
})
