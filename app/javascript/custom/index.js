document.addEventListener("turbolinks:load", function () {
  // create audio element and place on the page according to the window width
  let ww = window.innerWidth;
  const radio = document.createElement('audio');

  radio.src = "https://podcast-media.zenolive.com/media/agxzfnplbm8tc3RhdHNyFgsSCU1lZGlhSXRlbRiAgIi89pLjCQyiAQdsaWJyYXJ5.mp3";
  const topRadioHolder = document.querySelector('.topRadioHolder');
  const bottomRadioHolder = document.querySelector('.bottomRadioHolder');

  ww >= 768 ? topRadioHolder.appendChild(radio) : bottomRadioHolder.appendChild(radio); 

  import("./audioplayer").then(() => {
    $("audio").audioPlayer();
  });
  import("./jquery.nicescroll").then(() => {
    $(".tab2").niceScroll();
    $("#radiochat").getNiceScroll(0).doScrollTop(10000);
      });
  

  // add event on the chat tab
  $(".tab2").click(() => $("#radiochat").getNiceScroll(0).doScrollTop(100000));


});