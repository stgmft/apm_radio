class Message < ApplicationRecord
  belongs_to :chatroom
  belongs_to :user
  validates :body, presence: true, length: {maximum: 355}
  after_create_commit { MessageBroadcastJob.perform_later self }
end
