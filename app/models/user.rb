class User < ApplicationRecord
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save {self.email = email.downcase}
  before_create :create_activation_digest
  validates :name, presence: true, length: {maximum: 50},
  uniqueness: {case_sensitive: false, message: 'Пользователь с именем %{value} уже существует.'}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255},
            format: {with: VALID_EMAIL_REGEX}
  has_secure_password
  validates :password, presence: true, length: {minimum: 6}

  # Returns the hash digest of the given string (user.remember)
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions
  def remember
    self.remember_token = User.new_token
    # Rails.logger.info "Called user.remember"
    update_attribute(:remember_digest, User.digest(remember_token))
    remember_digest
  end

  def session_token
    remember_digest || remember
  end

  # Forgets a user
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Returns true if the given token matches the digest
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Activates account
  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end

  def appear
    unless $redis.hset("u_ol", self.name, self.id) == 0
      data = { action: "appear", appeared_user: self.name }
      Rails.logger.info "APPEAR METHOD sent data: #{data.inspect} time: #{Time.now}"
      ActionCable.server.broadcast("appearance_channel", data)
    end
  end

  def disappear
    $redis.hdel("u_ol", self.name)
    data = { action: "disappear", disappeared_user: self.name }
    Rails.logger.info "DISAPPEAR METHOD CALLED for #{self.name} time: #{Time.now}"
    ActionCable.server.broadcast("appearance_channel", data)
  end

  private

  # Creates and assigns the activation token and digest
  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

end
