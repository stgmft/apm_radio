class StaticPagesController < ApplicationController
  def home
    @users_online = $redis.hkeys('u_ol')
    @chatroom = Chatroom.last
    @messages = Message.where(chatroom_id: @chatroom.id).includes(:user)
  end

  def archive
  end
end
