class MessageBroadcastJob < ActiveJob::Base
  queue_as :default

  def perform(message)
    chatroom ||= Chatroom.find_by(id: message.chatroom_id)
    data = {}
    data['body'] = message.body
    data['sent_by'] = User.find_by(id: message.user_id).name
    Rails.logger.info "PERFORM METHOD OF MessageBroadcastJob"
    ChatChannel.broadcast_to(chatroom, data)
  end
end
