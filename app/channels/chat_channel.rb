class ChatChannel < ApplicationCable::Channel
  def subscribed
    @chatroom = Chatroom.last
    if @chatroom
      stream_for @chatroom
    else
      reject
    end
  end

  def receive(data)
    Message.create!(body: data['body'], user_id: current_user.id, chatroom_id: @chatroom.id )
    # Rails.logger.info "Message is created #{data}"  
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
