class AppearanceChannel < ApplicationCable::Channel
  def subscribed
    stream_from "appearance_channel"
  end

  def unsubscribed
    current_user.disappear
  end

  def appear()
    current_user.appear
    Rails.logger.info "Appear called on #{current_user.name}"
  end

  def away()
    current_user.disappear
  end

end
