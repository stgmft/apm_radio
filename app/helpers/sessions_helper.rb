module SessionsHelper

  # Logs in the given user
  def log_in(user)
    # for use in ActionCable connect methog
    cookies.encrypted[:user_name] = user.name
    session[:user_id] = user.id
    session[:session_token] = user.session_token
  end

  # Remember a user in a persistent session
  def remember(user)
    user.remember
    Rails.logger.info "CALLED REMEMBER USER"
    cookies.permanent.encrypted[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  #Returns the user corresponding to the remember token cookie
  def current_user
    if (user_id = session[:user_id])
      user = User.find_by(id: user_id)
      if user && session[:session_token] == user.session_token
        @current_user = user
      end
    elsif (user_id = cookies.encrypted[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  def current_user?(user)
    user == current_user
  end

  # Forgets a persistent session
  def forget(user)
    user.forget
    cookies.delete(:user_name)
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # Returns true if the user is logged-in, false otherwise
  def logged_in?
    !current_user.nil?
  end

  # Logs out the current user
  def log_out
    forget(current_user)
    reset_session
    @current_user = nil
  end

  # Stores the URL trying to be accessed
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

end
