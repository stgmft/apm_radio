# User.create!(name: "Admin User",
#             email: "example@apm.org",
#             password: "password",
#             password_confirmation: "password",
#             admin: true,
#             activated: true,
#             activated_at: Time.zone.now)

49.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@apm.org"
  password = "password"
  user = User.create!(name: name,
              email: email,
              password: password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
  # if (n < 6)
  #   Message.create!(body: Faker::Hipster.sentence(word_count: 4),
  #                   user_id: user.id,
  #                   chatroom_id: "2",
  #                   created_at: Time.zone.now)
  # end
end
