require 'rails_helper'

feature "ChatPresences", type: :feature do

  let!(:user) {FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)}
<<<<<<< HEAD

=======
>>>>>>> chat_channel
  scenario "unauthorised user visits main page" do
    visit root_path
    expect(page).not_to have_selector(".radiochat")
  end

  scenario "authorised user visits main page" do
    visit login_path
    fill_in('session[email]', :with => user.email)
    fill_in('session[password]', :with => user.password)
    click_button('Авторизоваться')
    visit root_path
    expect(page).to have_selector(".radiochat")
  end
end
