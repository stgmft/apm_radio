require 'rails_helper'

RSpec.feature "persistent cookies", type: :feature do

  let(:user) {FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)}

  it "not set when check box is not checked" do
    visit '/login'
    fill_in "Email",id:"session_email", :with => user.email
    fill_in "session[password]", :with => user.password
    click_on 'Авторизоваться'
    expect(page).not_to have_link(href: login_path)
    # emulates browser close
    delete_cookie('_apm_radio_session')
    visit '/'
    # show_me_the_cookies
    expect(page).to have_link(href: login_path)
  end

  it "are set when check box is checked" do
    visit '/login'
    fill_in "Email",id:"session_email", :with => user.email
    fill_in "session[password]", :with => user.password
    check "Запомнить меня"
    click_on 'Авторизоваться'
    # emulates browser close
    delete_cookie('_apm_radio_session')
    # show_me_the_cookies
    expect(page).not_to have_link(href: login_path)
  end
end
