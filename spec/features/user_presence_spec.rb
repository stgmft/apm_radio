require 'rails_helper'

RSpec.feature "AppearanceChannel", type: :feature do

  before do
    Capybara.default_driver = :selenium_chrome
  end


  let!(:session) { Capybara::Session.new(:selenium_chrome) }
  let!(:session1) { Capybara::Session.new(:selenium_chrome) }

  scenario "after login users appears in radiochat" do
    user1 = User.create(name: Faker::Name.name, email: Faker::Internet.email, password: 'password')#FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)
    user2 = User.create(name: Faker::Name.name, email: Faker::Internet.email, password: 'password')#FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)
    session1.visit 'http://localhost:3000/login'
    session1.fill_in "Email",id:"session_email", :with => user1.email
    session1.fill_in "session[password]", :with => user1.password
    session1.click_button 'Авторизоваться'
    # expect(page.current_path).to eq '/'
    ul = page.find('ul.users-online')
    users_online = ul.all('span').map{|i| i.text}
    expect(users_online.include?(user1.name)).to be_truthy

    # Capybara.using_session(session) do
      # user2 = FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)
      session.visit 'http://localhost:3000/login'
      session.click_link('Войти')
      # expect(page).to have_link(href: login_path)

      session.fill_in "Email",id:"session_email", :with => user2.email
      session.fill_in "session[password]", :with => user2.password
      session.click_button 'Авторизоваться'
      Capybara::Screenshot.screenshot_and_save_page
      expect(page.current_path).to eq '/'
      puts "user1: #{user1.name}, user2: #{user2.name}, users: #{users_online}"

      expect(users_online.include?(user1.name)).to be_truthy
      expect(users_online.include?(user2.name)).to be_truthy
    # end
  end

end
