require 'rails_helper'

RSpec.describe User, type: :model do
  it "should save a user with valid data" do
    user = User.new(name: "Alice", email: "alice@mail.org", password: "password")
    expect(user.valid?).to be_truthy
  end
end
