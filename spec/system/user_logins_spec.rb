require 'rails_helper'

RSpec.describe "User logins", type: :system do

  def log_in_as(user)
    visit login_path
    fill_in "session[email]", :with => user.email
    fill_in "session[password]", :with => user.password
    click_button "Авторизоваться"
  end

<<<<<<< HEAD
  let(:user) {FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)}
=======
  let!(:user) { FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email) }
  let!(:chatroom) { FactoryBot.create(:chatroom, title: Faker::Name.name) }
>>>>>>> chat_channel

  before do
    driven_by(:rack_test)
  end

  it "with valid data" do
    log_in_as(user)
    expect(current_path).to eq(root_path)
    expect(page).not_to have_selector('.alert-danger')
    expect(page).not_to have_link(href: login_path)
    within('.navbar') do
      expect(page).to have_link('Профиль', href: user_path(user))
      expect(page).to have_link('Настройки')
      expect(page).to have_link('Выйти', href: logout_path)
    end
  end

  it "with invalid email" do
    user = FactoryBot.build(:user, name: Faker::Name.name, email: "not_valid@email.com")
    log_in_as(user)
    expect(page).to have_selector('.alert-danger')
    expect(current_path).to eq(login_path)
  end

  it "with invalid password" do
    user = FactoryBot.build(:user, name: Faker::Name.name, password: "not_valid_password")
    log_in_as(user)
    expect(page).to have_selector('.alert-danger')
    expect(current_path).to eq(login_path)
  end
end
