require 'rails_helper'

RSpec.describe "user", type: :system do

  before do
    driven_by(:rack_test)
    visit '/signup'
  end

  let(:user) { FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email) }

  it 'signs up with valid data' do
    fill_in 'Name', with: Faker::Name.name
    fill_in 'Email', with: Faker::Internet.email
    fill_in 'Password', with: 'password'
    fill_in 'Confirmation', with: 'password'
    click_button 'Зарегистрироваться'
    expect(page).not_to have_selector('.alert-danger')
    expect(page).to have_current_path(/users\/[0-9]+/)
  end

  it 'signs up with name that already exists' do
    fill_in 'Name', with: user.name
    fill_in 'Email', with: Faker::Internet.email
    fill_in 'Password', with: 'password'
    fill_in 'Confirmation', with: 'password'
    click_button 'Зарегистрироваться'
    expect(page).to have_selector('.alert-danger')
  end

  it 'signs up with invalid email' do
    fill_in 'Name', with: Faker::Name.name
    fill_in 'Email', with: 'wrong@email'
    fill_in 'Password', with: 'password'
    fill_in 'Confirmation', with: 'password'
    click_button 'Зарегистрироваться'
    expect(page).to have_selector('.alert-danger')
  end

  it 'signs up with not matching passwords' do
    fill_in 'Name', with: Faker::Name.name
    fill_in 'Email', with: Faker::Internet.email
    fill_in 'Password', with: 'password'
    fill_in 'Confirmation', with: 'passwo1d'
    click_button 'Зарегистрироваться'
    expect(page).to have_selector('.alert-danger')
  end

end
