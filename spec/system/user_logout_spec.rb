require 'rails_helper'

RSpec.describe "User logout", type: :system do

  before do
    driven_by(:rack_test)
    user = FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)
    visit login_path
    fill_in "session[email]", :with => user.email
    fill_in "session[password]", :with => user.password
    click_button "Авторизоваться"
  end

  it 'this changes links in navbar' do
    click_on "Выйти"
    within('.navbar') do
      expect(page).to have_link(href: login_path)
      expect(page).to have_link(href: signup_path)
    end
  end

  it 'this redirects to main page and hides chat' do
    visit 'archive'
    click_on "Выйти"
    expect(current_path).to eq(root_path)
    expect(page).not_to have_selector('.radiochat')
  end

end
