require 'rails_helper'

RSpec.describe "user sign up and sign in", type: :request do
  describe "user's sign up" do
    it "sign up with valid data" do
      get signup_path
      expect(response).to have_http_status(200)
      post users_path, params: {user: {name: "Ioann", email: "io@mail.com", password: "password"}}
      follow_redirect!
      expect(response).to render_template('show')
      expect(response.body).to include("Регистрация прошла успешно!")
    end
  end
end
