FactoryBot.define do
  factory :user do
    name {"Random User"}
    email {"random_user@email.com"}
    password {"password"}
  end 
end
