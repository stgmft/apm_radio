require 'rails_helper'

RSpec.describe ApplicationCable::Connection, type: :channel do

  user = FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)

  it "successfully connects" do
    cookies.encrypted[:user_name] = user.name
    expect { connect }.not_to raise_error
  end

  it "rejects connection" do
    expect { connect }.to have_rejected_connection
  end

end
