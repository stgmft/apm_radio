require 'rails_helper'

RSpec.describe AppearanceChannel, type: :channel do

  user = FactoryBot.create(:user, name: Faker::Name.name, email: Faker::Internet.email)

  it "successfully subscribes" do
    subscribe
    expect(subscription).to be_confirmed
    expect(subscription).to have_stream_from("appearance_channel")
  end

  it "broadcasts to channel" do
    expect {
      ActionCable.server.broadcast(
        "appearance_channel", {user: user.name})
      }.to have_broadcasted_to("appearance_channel").with(user: user.name)
  end

end
