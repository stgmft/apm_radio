require 'rails_helper'

RSpec.describe RadiochatChannel do

  it "successfully subscribes" do
    subscribe room_id: "radiochat_channel"

    expect(subscription).to be_confirmed
    expect(subscription).to have_stream_from("radiochat_channel")
  end

end
